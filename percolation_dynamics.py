import numpy as np
import time
import os
import socket
import logging
import copy
import odespy
import random
import pickle
from mimura_murray import *
from graph_definitions import triangle_lattice, ba_graph
from scipy.integrate import odeint, ode 
# import parallel modules 
from easy_parallel import * 

def pattern_under_percolation(problem, t_final, N_mc=1, x0=None):
    ''' Returns the norms (l1, l2, linf) of long term Turing patterns, changing under bond percolation '''
    np.random.seed()
    num_edges = problem.G.number_of_edges()
    # If no inintial condition is given use a random initial condition
    if x0 == None:
        x0 = problem.random_initial_condition(N=N_mc*(num_edges+1))
    T = np.linspace(0,t_final,2) # Only save values at t=0 and t=t_final
    pattern_norm_l1 = np.zeros((num_edges+1,N_mc))
    pattern_norm_l2 = np.zeros((num_edges+1,N_mc))
    pattern_norm_linf = np.zeros((num_edges+1,N_mc))
    # Loop over number of MC simulations
    for n in range(N_mc):
        problem_copy = copy.deepcopy(problem)
        # Solve using scipy (note: not suitable for parallel computation)
#        sol = odeint(problem_copy.f, x0[n*num_edges], T, full_output=False, rtol=1.5e-8, atol=1.5e-8, mxstep=10000)
        # Solve using odespy (note: seems suitable for parallel computation)
        solver = odespy.Lsoda(problem_copy.f, rtol=1.5e-8, atol=1.5e-8, jac=None, nsteps=100000)
        solver.set_initial_condition(x0[n*num_edges])
        sol, tt = solver.solve(T)
        # Save the norm of the solution
        pattern_norm_l1[0,n] = np.linalg.norm(sol[-1,:]-problem.steady_state(),ord=1)
        pattern_norm_l2[0,n] = np.linalg.norm(sol[-1,:]-problem.steady_state(),ord=2)
        pattern_norm_linf[0,n] = np.linalg.norm(sol[-1,:]-problem.steady_state(),ord=np.inf)
        # Remove a random edge one by one and record what happens to the Turing patterns
        for edge_count in range(1,num_edges+1):
            problem_copy.remove_random_bonds()
            # Solve using scipy (note: not suitable for parallel computation)
#            sol = odeint(problem_copy.f, x0[n+num_edges], T, full_output=False, rtol=1.5e-8, atol=1.5e-8, mxstep=10000)
            # Solve using odespy (note: seems suitable for parallel computation)
            solver = odespy.Lsoda(problem_copy.f, rtol=1.5e-8, atol=1.5e-8, jac=None, nsteps=100000)
            solver.set_initial_condition(x0[n+edge_count])
            sol, tt = solver.solve(T)
            # Save the norm of the solution
            pattern_norm_l1[edge_count,n] = np.linalg.norm(sol[-1,:]-problem.steady_state(),ord=1)
            pattern_norm_l2[edge_count,n] = np.linalg.norm(sol[-1,:]-problem.steady_state(),ord=2)
            pattern_norm_linf[edge_count,n] = np.linalg.norm(sol[-1,:]-problem.steady_state(),ord=np.inf)
    return pattern_norm_l1, pattern_norm_l2, pattern_norm_linf

def spectrum_under_percolation(problem, t_final=0, N_mc=1, x0=None):
    ''' Returns the Laplacian eigenvalue spectrum, changing under bond percolation '''
    np.random.seed()
    num_edges = problem.G.number_of_edges()
    num_nodes = problem.G.number_of_nodes()
    spectrum = np.zeros((num_edges+1,N_mc,num_nodes))
    for n in range(N_mc):
        problem_copy = copy.deepcopy(problem)
        spectrum[0,n] = nx.laplacian_spectrum(problem_copy.G)
        for edge_count in range(1,num_edges+1):
            problem_copy.remove_random_bonds()
            spectrum[edge_count,n] = nx.laplacian_spectrum(problem_copy.G) 
    return spectrum


## Define the parameters for Mimura-Murray
a = 35.0; b = 16.0; c = 9.0; d = 2.0/5.0
## Define the diffusion parameters
epsilon = 0.12
sigma = 25.0

## Network definition
np.random.seed(42) # set the random seed
num_nodes = 20 # Number of nodes
## Complete graph
G = nx.complete_graph(num_nodes)
## Rectangular lattice grid
G = nx.convert_node_labels_to_integers(nx.grid_2d_graph(4,5))
## Triangle lattice
G = triangle_lattice(4,5)
## Barabasi-Albert graph
deg_attach = 2
# initial complete graph
G = ba_graph(num_nodes, deg_attach)
# initial empty graph
G = nx.barabasi_albert_graph(num_nodes, deg_attach)

# Laplacian matrix of the graph G
L = nx.laplacian_matrix(G)

## Create instance of the model
problem = MimuraMurray(35.0, 16.0, 9.0, 2.0/5.0, G, L, epsilon, sigma)
## Parameter for time integration
t_final = 1000

## Logging information
today = time.strftime('%d-%m-%Y')
time_point = time.strftime('%H.%M.%S')
#logging.getLogger().addHandler(logging.StreamHandler()) # Add logging info to stdout
logger = logging.getLogger(__name__)
logging.basicConfig(filename=os.path.join(os.getcwd(),'Data', 'log_files', 'num_nodes_'+
        str(num_nodes)+'_epsilon_%.2f'%(epsilon)+'.log'),
        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
        datefmt='%H:%M:%S',
        level=logging.INFO)
logger.info('*************************************************')
logger.info('Process running on machine %s' % socket.gethostname())
logger.info('Process started at ' + today + ' at ' + time_point)
logger.info('-------------------------')
logger.info('Graph G with number of nodes: %d' % num_nodes)
logger.info('Graph G with number of edges: %d' % G.number_of_edges())
logger.info('Final time : %f' % t_final)
logger.info('Epsilon : %f' % epsilon)
logger.info('Sigma : %f' % sigma)
## Set up save directory
save_dir = os.path.join(os.getcwd(),'Data',today+'_'+time_point)
os.makedirs(save_dir)
# Save the graph to a text file
pickle.dump(G, open(os.path.join(save_dir,'graph.txt'), 'w'))

# Perform a Monte Carlo simulation
### MC run
N_pool = 30 # Number of machine cores
N_mc = 500  # number of MC per core
logger.info('Total number of samples %d' % (N_pool*N_mc))
t0 = time.time()
## Look at the Turing pattern norms (takes long time)
#result = mp_parallel(pattern_under_percolation, N_pool, N_pool*[problem], N_pool*[t_final], N_pool*[N_mc])
#np.save(os.path.join(save_dir,'num_nodes_'+str(num_nodes))+'_epsilon_%.2f'%(epsilon), result)
## Look at the eigenvalue spectrum (quicker)
#result_spectrum = mp_parallel(spectrum_under_percolation, N_pool, N_pool*[problem], N_pool*[t_final], N_pool*[N_mc])
#np.save(os.path.join(save_dir,'spectrum_num_nodes_'+str(num_nodes)), result_spectrum)
t1 = time.time()
logger.info('Finish updating log, runtime %f s' %(t1-t0))
logger.info('*************************************************')
