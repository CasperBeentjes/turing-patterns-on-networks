import numpy as np
import os
import matplotlib.pyplot as plt
from scipy.stats import binom

cur_dir = os.getcwd()

## Analyse the extremal eigenvalues of graphs under percolation
## Data files with num_nodes:   100 - Rectangular lattice (20 nodes)
##                              110 - Triangular lattice (20 nodes)
##                              120 - Barabasi-albert graph (20 nodes)

num_nodes = 100

if num_nodes == 100:
    num_edges=31 # Rectangular lattice graph
elif num_nodes == 110:
    num_edges=43 # Triangular lattice graph
elif num_nodes == 120:
    num_edges=37 # Barabasi-Albert (20,2) graph
else:
    num_edges = num_nodes*(num_nodes-1)/2 # complete graph

# Load the data from the files
data = np.load(cur_dir+'/Data/'+'spectrum_num_nodes_'+str(num_nodes))
data = np.hstack(data) # Combine data from multiple cores

# If we don't have a complete graph set correct number of nodes again
if num_edges != num_nodes*(num_nodes-1)/2:
    num_nodes = 20

# Find the maximal value in the data
l_max = np.amax(data,axis=2)
# Below which value do we classify an eigenvalue to be zero?
threshold = 1e-14 
# Set all values below threshold to the treshold
data = np.clip(data,threshold,num_nodes)
# For the Fiedler eigenvalue we want the smallest eigenvalue > threshold
# Therefore we map threshold to infinity temporarily
data[np.where(data == threshold)] = np.inf
# Determine the Fiedler eigenvalue
l_min = np.amin(data,axis=2)
# Restore values to correct threshold
l_min[np.where(l_min == np.inf)] = threshold
data[np.where(data == np.inf)] = threshold

# Plotting range
data_min = 0.0
data_max = np.max(l_max)+1

# Determine emperical distrubtion of the eigenvalues over the data
num_bins = 200
H_min = np.zeros((num_edges+1, num_bins))
H_max = np.zeros((num_edges+1, num_bins))
for i in range(num_edges+1):
    H_min[i], bin_edges = np.histogram(l_min[i],range=(data_min,data_max),bins=num_bins,density=False)
    H_max[i], bin_edges = np.histogram(l_max[i],range=(data_min,data_max),bins=num_bins,density=False)

# Normalise with respect to the number of Monte Carlo trials
H_min = H_min/np.shape(l_min)[1]
H_max = H_max/np.shape(l_max)[1]

# Determine the mean of the eigenvalues over the data
H_mean_min = np.mean(l_min,axis=1)
H_mean_max = np.mean(l_max,axis=1)

# Convert the observations to occupation probability
# Use binomial distribution to get correct value for each oc. prob. p
num_plot = 500
P = np.linspace(0,1,num_plot)

p_min = np.zeros((num_plot, num_bins))
p_max = np.zeros((num_plot, num_bins))
mean_min = np.zeros(num_plot)
mean_max = np.zeros(num_plot)

for i, p in enumerate(P):
    p_min[i] = binom.pmf(range(num_edges+1),num_edges,p).dot(H_min)
    p_max[i] = binom.pmf(range(num_edges+1),num_edges,p).dot(H_max)
    mean_min[i] = binom.pmf(range(num_edges+1),num_edges,p).dot(H_mean_min)
    mean_max[i] = binom.pmf(range(num_edges+1),num_edges,p).dot(H_mean_max)


## Plotting
# Distribution plot of Fiedler eigenvalue
X,Y = np.meshgrid(1-P, np.linspace(data_min,data_max,num_bins))
Z = np.transpose(p_min)
plt.pcolormesh(X,Y,Z,cmap='inferno_r',vmin=0,vmax=np.ceil(np.max(Z)))
plt.colorbar()
plt.xlim([0,1.0])
plt.ylim([0,data_max])
plt.ylabel(r'$\Lambda$',fontsize=24)
plt.xlabel(r'Occupation probability $\phi$',fontsize=24)
plt.tick_params(axis='both', which='major', labelsize=16)
plt.title('Distribution of Fiedler eigenvalue')
plt.figure()
# Distribution plot of maximal eigenvalue
X,Y = np.meshgrid(1-P, np.linspace(data_min,data_max,num_bins))
Z = np.transpose(p_max)
plt.pcolormesh(X,Y,Z,cmap='inferno_r',vmin=0,vmax=np.ceil(np.max(Z)))
plt.colorbar()
plt.xlim([0,1.0])
plt.ylim([0,data_max])
plt.ylabel(r'$\Lambda$',fontsize=24)
plt.xlabel(r'Occupation probability $\phi$',fontsize=24)
plt.tick_params(axis='both', which='major', labelsize=16)
plt.title('Distribution of maximal eigenvalue')
plt.figure()
# Mean behaviour of Fiedler & maximal eigenvalue
plt.plot(1-P,mean_min,'k',label=r'$\Lambda_2$')
plt.plot(1-P,mean_max,'r',label=r'$\Lambda_N$')
plt.ylabel(r'$\Lambda$',fontsize=24)
plt.xlabel(r'Occupation probability $\phi$',fontsize=24)
plt.tick_params(axis='both', which='major', labelsize=16)
plt.xlim([0,1.0])
plt.ylim([0,data_max])
plt.legend(loc='upper left',fontsize=20)
fig = plt.gcf()
fig.set_size_inches(10,7)
#plt.savefig('spectrum_complete.pdf')
plt.show()
