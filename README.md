# README #

### What is this repository for? ###

* Python implementation of reaction-diffusion equations on networks. See [essay](https://cbeentjes.github.io/ramblings/) for a more detailed exposure.
* Current reaction model included:

1 Mimura-Murray model (http://www.sciencedirect.com/science/article/pii/0022519378903326?via%3Dihub)

### How do I get set up? ###

* Writing an own model can be done following the example in the Mimura-Murray model file.

### How do I use the Python scripts? ###
* mimura_murray - contains the class with the mimura-murray dynamcis
* mode_selection - solve the reaction-diffusion ODE and decompose the resulting solution onto the eigenvector basis of the Laplacian matrix of the graph
* pattern_analysis - file used to generate the analysis of the norms of the Turing patterns in the essay.
* percolation_dynamics - run a bond percolation and look at the resulting 1) Turing pattern 2) Laplacian spectrum
* spectrum_analysis - file used to analyse the Laplacian spectrum under bond percolation

Helper functions:

* network_pattern - file with the basic class for reaction-diffusion ODEs on networks. Contains many helper functions for plotting as well
* easy_parallel - run processes on multiple cores
* graph_definitions - construct several graphs not included in NetworkX package
* spectrum_ba_graph - look at the spectrum of a Barabasi-Albert graph generated in two different ways as in Appendix B of the paper.

### Who do I talk to? ###

* Casper Beentjes [beentjes@maths.ox.ac.uk](mailto:beentjes@maths.ox.ac.uk)