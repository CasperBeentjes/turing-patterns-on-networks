import numpy as np
import os
import matplotlib.pyplot as plt
from scipy.stats import binom

cur_dir = os.getcwd()

# Diffusion parameters
sigma = 25.0
epsilon = 0.12

## Analyse the norm (l1,l2,linf) of Turing patterns of graphs under percolation
## Data files with num_nodes:   100 - Rectangular lattice (20 nodes)
##                              110 - Triangular lattice (20 nodes)
##                              120 - Barabasi-albert graph (20 nodes)

# Loop over different graphs
N_nodes = [120,110,100,20]
# Legend for the plotting
leg = [r'Barabasi-Albert graph','Triangular lattice','Rectangular lattice','Complete graph']
for k, num_nodes in enumerate(N_nodes):
    try:
        # Load the data from the files
        data = np.load(cur_dir+'/Data/'+'num_nodes_'+str(num_nodes)+'_epsilon_%.2f.npy'%epsilon)
        data_l1, data_l2, data_linf = np.hsplit(data,3)

        if num_nodes == 100:
            num_edges=31 # Rectangular lattice graph
        elif num_nodes == 110:
            num_edges=43 # Triangular lattice graph
        elif num_nodes == 120:
            num_edges=37 # Barabasi-Albert (20,2) graph
        else:
            num_edges = num_nodes*(num_nodes-1)/2 # complete graph
        # If we don't have a complete graph set correct number of nodes again
        if num_edges != num_nodes*(num_nodes-1)/2:
            num_nodes = 20
        
        # Combine data from multiple cores 
        data_l1 = np.hstack(np.squeeze(data_l1))
        data_l2 = np.hstack(np.squeeze(data_l2))
        data_linf = np.hstack(np.squeeze(data_linf))

        # Choose which data to plot
        data = data_l2

        # Plotting range
        data_min = 0.0
        data_max = np.max(data)+1

        # Determine emperical distrubtion of the pattern over the data
        num_bins = 100
        H = np.zeros((num_edges+1, num_bins))

        for i in range(num_edges+1):
            H[i], bin_edges = np.histogram(data[i],range=(data_min,data_max),bins=num_bins)
        
        # Normalise with respect to the number of Monte Carlo trials
        H = H/np.shape(data)[1]

        # Determine the mean behaviour
        H_mean = np.mean(data,axis=1)

        # Convert the observations to occupation probability
        # Use binomial distribution to get correct value for each oc. prob. p
        num_plot = 1000
        P = np.linspace(0,1,num_plot)

        p_norm = np.zeros((num_plot, num_bins))
        mean_norm = np.zeros(num_plot)

        for i, p in enumerate(P):
            p_norm[i] = binom.pmf(range(num_edges+1),num_edges,p).dot(H)
            mean_norm[i] = binom.pmf(range(num_edges+1),num_edges,p).dot(H_mean)

        plt.plot(1-P,mean_norm,label=leg[k])
    except:
        continue

## Plotting
# Mean behaviour plot details
plt.ylabel(r'$||(u,v)-(u^*,v^*)||_{2}$',fontsize=24)
plt.xlabel(r'Occupation probability $\phi$',fontsize=24)
plt.tick_params(axis='both', which='major', labelsize=16)
plt.xlim([0,1.0])
plt.ylim([0,data_max])
plt.legend(fontsize=16)
fig = plt.gcf()
fig.set_size_inches(10,7)
#plt.savefig('2norm_eps1.pdf')
plt.figure()
# Distribution of pattern, last one in the loop above
X,Y = np.meshgrid(1-P, np.linspace(data_min,data_max,num_bins))
Z = np.transpose(p_norm)
plt.pcolormesh(X,Y,Z,cmap='inferno_r',vmin=0,vmax=1)
plt.colorbar()
plt.xlim([0,1.0])
plt.ylim([0,data_max])
plt.ylabel(r'$||(u,v)-(u^*,v^*)||_{2}$',fontsize=24)
plt.xlabel(r'Occupation probability $\phi$',fontsize=24)
plt.tick_params(axis='both', which='major', labelsize=16)
fig = plt.gcf()
fig.set_size_inches(10,7)
#plt.savefig('dist2normrectangle.pdf')
plt.show()
