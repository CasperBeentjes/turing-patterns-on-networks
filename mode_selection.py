import numpy as np
import time
import pdb 
import odespy
import random
from mimura_murray import *
from graph_definitions import triangle_lattice, ba_graph

## Look at the eigenvector decomposition of the long-term Turing patterns
## For some values we see that the Turing patterns are strongly aligned in one direction

## Define the parameters for Mimura-Murray
a = 35.0; b = 16.0; c = 9.0; d = 2.0/5.0
## Define the diffusion parameters
epsilon = 0.1
sigma = 25.0

## Network definition
np.random.seed(42) # set the random seed
num_nodes = 20 # Number of nodes
## Complete graph
G = nx.complete_graph(num_nodes)
## Rectangular lattice grid
G = nx.convert_node_labels_to_integers(nx.grid_2d_graph(4,5))
## Triangle lattice
G = triangle_lattice(4,5)
## Barabasi-Albert graph
deg_attach = 2
# initial complete graph
G = ba_graph(num_nodes, deg_attach)
# initial empty graph
G = nx.barabasi_albert_graph(num_nodes, deg_attach)

# Create the Laplacian matrix
L = nx.laplacian_matrix(G)

## Create instance of the model
np.random.seed(200) # set the random seed for consistency
problem = MimuraMurray(35.0, 16.0, 9.0, 2.0/5.0, G, L, epsilon, sigma)
## Parameter for time integration
t_final = 1000

# Solve the network ODE reaction diffusion system until t_final
T = np.linspace(0,t_final,2)
t0 = time.time()
# Use odespy LSODA solver
solver = odespy.Lsoda(problem.f, rtol=1.5e-8, atol=1.5e-8, jac=None, nsteps=100000)
solver.set_initial_condition(problem.random_initial_condition()[0])
sol, tt = solver.solve(T)
t1 = time.time()
print 'Time elapsed: %f s' %(t1-t0)

# Project the solution onto the eigenvectors of the Laplacian matrix
problem.plot_ev_coef((sol[-1]-problem.steady_state())[:num_nodes])
plt.ylabel(r'$|a_r|$',fontsize=24)
plt.xlabel(r'Eigenvalue index $r$',fontsize=24)
plt.tick_params(axis='both', which='major', labelsize=16)
#plt.savefig('decomp01.pdf')
plt.show()
