from network_pattern import *

class MimuraMurray(NetworkODE):
    ''' Mimura-Murray dynamics on a network G
    
    Built upon the NetworkODE meta-class

    Args:
        a,b,c,d     : Mimura-Murray parameters
        G           : graph on which system is defined
        L           : network Laplacian matrix
        epsilon     : diffusion strength
        sigma       : activator/inhibitor diffusion ratio
        spectrum    : Laplacian eigenvalues
    '''

    def __init__(self, a, b, c, d, G, L=None, epsilon=0.0, sigma=1.0, spectrum=None):
        NetworkODE.__init__(self, G, L, epsilon, sigma, spectrum)
        self.a = a
        self.b = b
        self.c = c
        self.d = d

    def steady_state(self):
        'Return the non-trivial homogeneous steady-state of the model'
        a, b, c, d = self.a, self.b, self.c, self.d
        num_nodes = self.G.number_of_nodes()
        u_ss = -0.5*c*(-(b/c - 1.0/d) - ((b/c - 1.0/d)**2 - 4*(-1.0/c)*(a/c + 1.0/d))**0.5)
        v_ss = (u_ss - 1.0)/d
        return np.append(np.full(num_nodes, u_ss), np.full(num_nodes, v_ss))

    def activator(self, u, v, t=0):
        'Return the activator dynamics on each node'
        a, b, c, d = self.a, self.b, self.c, self.d
        return ( (a+b*u-u**2)/c - v )*u

    def inhibitor(self, u, v, t=0):
        'Return the inhibitor dynamics on each node'
        a, b, c, d = self.a, self.b, self.c, self.d
        return ( u - (1 + d*v) )*v

    def reaction_jac(self, u, v, t=0):
        'Return the Jacobian entries of the reaction part'
        a, b, c, d = self.a, self.b, self.c, self.d
        f_u = (a+2*b*u-3*u**2)/c - v
        f_v = -u
        g_u = v
        g_v = u - 1 - 2*d*v
        return f_u, f_v, g_u, g_v 
