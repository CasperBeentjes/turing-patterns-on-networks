import networkx as nx
import numpy as np
import matplotlib.pyplot as plt

def _random_subset(seq,m):
    """ Return m unique elements from seq.

    This differs from random.sample which can return repeated
    elements if seq holds repeated elements.
    """
    targets=set()
    while len(targets)<m:
        x=np.random.choice(seq)
        targets.add(x)
    return targets

def ba_graph(n,m):
    '''Barabasi-Albert graph starting with complete graph'''
    # Add m initial nodes (m0 in barabasi-speak)
    G=nx.complete_graph(m)
    G.name="barabasi_albert_graph_complete(%s,%s)"%(n,m)
    # Target nodes for new edges
    targets=list(range(m))
    # List of existing nodes, with nodes repeated once for each adjacent edge
    repeated_nodes=[]
    # Start adding the other n-m nodes. The first node is m.
    source=m
    while source<n:
        # Add edges to m nodes from the source.
        G.add_edges_from(zip([source]*m,targets))
        # Add one node to the list for each new edge just created.
        repeated_nodes.extend(targets)
        # And the new node "source" has m edges to add to the list.
        repeated_nodes.extend([source]*m)
        # Now choose m unique nodes from the existing nodes
        # Pick uniformly from repeated_nodes (preferential attachement)
        targets = _random_subset(repeated_nodes,m)
        source += 1
    return G

def triangle_lattice(n,m):
    ''' Graph from a triangular lattice'''
    G = nx.grid_2d_graph(n,m)
    G.name="triangle_lattice_graph(%s,%s)"%(n,m)
    for node in G:
        x, y = node
        if x>0 and y>0:
            G.add_edge(node,(x-1,y-1))
    G = nx.convert_node_labels_to_integers(G)
    return G
