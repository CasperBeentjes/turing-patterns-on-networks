## Functions for embarrasingly parallelisable code
import itertools
import multiprocessing
import numpy as np

# Unpack function arguments
def parfunc(input_pair):
    func, args = input_pair
    return func(*args)

# Create single iterable for pool.map()
def pool_args(func, *args):
#    print zip(itertools.repeat(func), itertools.izip_longest(*args, fillvalue=None))
    return zip(itertools.repeat(func), itertools.izip_longest(*args, fillvalue=None))

# Run function with an arbitrary number of input arguments in parallel
# and combine the results in a numpy array afterwards
def mp_parallel(func, N_pool=None, *args):
    pool = multiprocessing.Pool(N_pool)
    result = pool.map(parfunc, pool_args(func, *args))
    cleaned = [x for x in result if not x is None]
    cleaned = np.asarray(cleaned)
    pool.close()
    pool.join()
    return cleaned
