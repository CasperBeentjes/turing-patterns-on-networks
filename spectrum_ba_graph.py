import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import time
import random
from easy_parallel import *
from graph_definitions import *

# Determine the extreme eigenvalues of a BA-graph with initial empty core
def extreme_ev(n,m,N):
    np.random.seed()
    A = np.zeros(N)
    B = np.zeros(N)
    for i in range(N): 
        G = nx.barabasi_albert_graph(n,m)
        S = nx.laplacian_spectrum(G)
        A[i] = S[1]
        B[i] = S[-1]
    return A, B

# Determine the extreme eigenvalues of a BA-graph with initial complete graph core
def extreme_ev_alt(n,m,N):
    np.random.seed()
    A = np.zeros(N)
    B = np.zeros(N)
    for i in range(N):
        G = ba_graph(n,m)
        S = nx.laplacian_spectrum(G)
        A[i] = S[1]
        B[i] = S[-1]
    return A, B


if __name__ == "__main__":
    num_nodes = [20]

    N_pool = 30 # Number of cores on machine
    N = 30 # Number of MC trials per core

    t0 = time.time()
    for n in num_nodes:
        D = range(1,n)
        A_mean = np.zeros((2,len(D)))
        B_mean = np.zeros((2,len(D)))
        for i, m in enumerate(D):
            A = mp_parallel(extreme_ev, N_pool, N_pool*[n], N_pool*[m], N_pool*[N])
            A = np.hstack(A)
            A_mean[:,i] = np.mean(A,axis=1)
            B = mp_parallel(extreme_ev_alt, N_pool, N_pool*[n], N_pool*[m], N_pool*[N])
            B = np.hstack(B)
            B_mean[:,i] = np.mean(B,axis=1)
        plt.plot(D,A_mean[0],'ko',label=r'$\Lambda_2$',ms=5.0)
        plt.plot(D,B_mean[0],'ro',label=r'$\Lambda_2$',ms=5.0)
        plt.plot(D,A_mean[1],'kv',label=r'$\Lambda_n$',ms=5.0)
        plt.plot(D,B_mean[1],'rv',label=r'$\Lambda_n$',ms=5.0)
    t1 = time.time()
    print t1-t0
    plt.xlim([0,np.max(num_nodes)])
    plt.ylim([0,np.max(num_nodes)+2])
    plt.tick_params(axis='both', which='major', labelsize=16)
    plt.ylabel(r'$\Lambda$',fontsize=24)
    plt.xlabel(r'$m$',fontsize=24)
    plt.legend(loc='upper left')
    plt.show()
#    plt.savefig('BA_initial.pdf')
