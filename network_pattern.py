import abc
import networkx as nx
import numpy as np
import warnings # suppress np.sqrt warning
import matplotlib.pyplot as plt 
import matplotlib.cbook
from matplotlib import animation
from matplotlib.ticker import MaxNLocator
from scipy.optimize import brentq, newton
from random import sample, seed

class NetworkODE:
    '''Basic class for reaction-diffusion systems on a network
    
    Implementation of a two-species reaction diffusion system of the form
    u' = f(u,v) + epsilon*L*u
    v' = g(u,v) + epsilon*sigma*L*v

    Note:
        This is a meta class and therefore can only be used to initiate
        another class, which specifies the reaction dynamics

    Args:
        G (networkx graph)  : graph on which system is defined
        L                   : network Laplacian matrix
        epsilon             : diffusion strength
        sigma               : activator/inhibitor diffusion ratio
        spectrum            : Laplacian eigenvalues
    '''

    __metaclass__ = abc.ABCMeta
    def __init__(self, G, L=None, epsilon=0.0, sigma=1.0, spectrum=None):
        self.G = G
        if L == None:
            self.L = nx.laplacian_matrix(G)
        else:
            self.L = L
        if spectrum == None:
            self.spectrum = -nx.laplacian_spectrum(G)
        else:
            self.spectrum = spectrum
        self.epsilon = epsilon
        self.sigma = sigma
        self.num_nodes = G.number_of_nodes()
    
    @abc.abstractmethod
    def activator(self):
        return

    @abc.abstractmethod
    def inhibitor(self):
        pass

    @abc.abstractmethod
    def reaction_jac(self):
        pass

    @abc.abstractmethod
    def steady_state(self):
        pass

    def random_initial_condition(self, eta=0.1, N=1):
        'Return a random perturbation on the steady state of the system'
        x0 = self.steady_state()
        x0 = np.array([x0]) + eta*(np.random.rand(N,np.size(x0))-0.5)
        return x0

    def diffusion_part(self, u, v, t=0):
        'Return the diffusion part of the ODE right hand side'
        epsilon, sigma, L = self.epsilon, self.sigma, self.L
        M = epsilon*np.concatenate((L.dot(u), L.dot(sigma*v)))
        return M
    
    def reaction_part(self, u, v, t=0):
        'Return the reaction part of the ODE right hand side'
        F = np.concatenate((self.activator(u, v, t), self.inhibitor(u, v, t)))
        return F

    def f(self, U, t=0):
        'Right hand side function for the time evolution of the concentrations u,v'
        u = U[:self.num_nodes]
        v = U[self.num_nodes:]
        F = self.reaction_part(u, v, t)
        M = -self.diffusion_part(u, v) 
        return F + M

    def f_switch(self, t, U):
        'Right hand side function with the t and U argument switched'
        return self.f(U, t)

    def dynamics_jac(self, U, t=0):
        'Return the Jacobian of the reaction-diffusion ODE systems'
        sigma, epsilon, L = self.sigma, self.epsilon, self.L
        u = U[:self.num_nodes]
        v = U[self.num_nodes:]
        f_u, f_v, g_u, g_v = self.reaction_jac(u, v, t) 
        return np.bmat([[np.diag(f_u) - epsilon*L, np.diag(g_u)],
            [np.diag(f_v), np.diag(g_v) - sigma*epsilon*L]])

    def jac_switch(self, t, U):
        return self.dynamics_jac(U, t)

    def remove_random_bond(self):
        'Remove one of the edges in the graph G at random and change the Laplacian L'
        G, L = self.G, self.L
        list_of_edges = G.edges()
        edges_to_remove = sample(list_of_edges, 1)
        G.remove_edges_from(edges_to_remove)
        # Construct the Laplacian, not using nx.laplacian_matrix
        # because we only have to change 4 entries of the Laplacian
        edges_to_remove = edges_to_remove[0]
        L[edges_to_remove[0],edges_to_remove[1]]=0
        L[edges_to_remove[1],edges_to_remove[0]]=0
        L[edges_to_remove[0],edges_to_remove[0]]-=1
        L[edges_to_remove[1],edges_to_remove[1]]-=1
        return

    def remove_random_bonds(self, num_to_remove=1):
        'Remove num_to_remove of the edges in the graph G at random and change the Laplacian L'
        if num_to_remove == 1:
            self.remove_random_bond()
            return
        else:
            list_of_edges = G.edges()
            edges_to_remove = sample(list_of_edges, num_to_remove)
            G.remove_edges_from(edges_to_remove)
            # Construct the Laplacian
            L = nx.laplacian_matrix(G)
            return

    def remove_random_node(self):
        'Remove one of the nodes in the graph G at random and change the Laplacian L'
        G, L = self.G, self.L
        list_of_nodes = G.nodes()
        nodes_to_remove = sample(list_of_nodes, 1)
        G.remove_nodes_from(nodes_to_remove)
        L = nx.laplacian_matrix(G)
        return

    def growth_factor(self, spectrum=None, epsilon=None, sigma=None, t=0):
        'Return the growth factor for specified eigenvector directions'
        if sigma == None:
            sigma = self.sigma
        if spectrum == None:
            spectrum = self.spectrum
        if epsilon == None:
            epsilon = self.epsilon
        u, v = self.steady_state()[0], self.steady_state()[-1]
        f_u, f_v, g_u, g_v = self.reaction_jac(u, v, t) 
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            return 0.5*(f_u + g_v + (1+sigma)*epsilon*spectrum 
                    + np.sqrt(4*f_v*g_u + (f_u - g_v + (1-sigma)*epsilon*spectrum)**2))

    def equal_growth_factor(self, eig_v1, eig_v2, sigma, epsilon0=0.05, t=0):
        'Return the curve of equal growth rate for two given eigenvalues between eps_min and eps_mac'
        equal_lambda = lambda epsilon: ( self.growth_factor(eig_v1, epsilon, sigma, t) 
                - self.growth_factor(eig_v2, epsilon, sigma, t) )
        eps = newton(equal_lambda, epsilon0)
        return eps

    def bounding_curve(self, epsilon, eig_v, t=0):
        'Return the marginal stability curve for a given eigenvalue eig_v for an array of epsilon'
        u, v = self.steady_state()[0], self.steady_state()[-1]
        f_u, f_v, g_u, g_v = self.reaction_jac(u, v, t) 
        curve = -( (f_u*g_v - f_v*g_u) + epsilon*eig_v*g_v )/(epsilon**2*eig_v**2 + epsilon*eig_v*f_u)
        return curve

    def intersection_bounding_curve(self, eig_v1, eig_v2, eps_min=0.05, eps_max=10.0):
        'Return the point of intersection between two marginal stability curves'
        intersection = lambda epsilon: ( self.bounding_curve(epsilon, eig_v1) 
                - self.bounding_curve(epsilon, eig_v2) )
        epsilon = brentq(intersection, eps_min, eps_max)
        return epsilon

    def epsilon_curve(self, sigma, eig_v, sign=-1.0, t=0):
        'Return the marginal stability curve for a given eigenvalue eig_v for an array of sigma'
        u, v = self.steady_state()[0], self.steady_state()[-1]
        f_u, f_v, g_u, g_v = self.reaction_jac(u, v, t) 
        return 0.5/(sigma*eig_v)*(-(sigma*f_u+g_v) + sign*((sigma*f_u+g_v)**2-4*sigma*(f_u*g_v-f_v*g_u))**0.5)

    def epsilon_maximum(self, eig_v, t=0):
        'Return the maximum value of epsilon on the marginal stab curve for a given eigenvalue eig_v'
        u, v = self.steady_state()[0], self.steady_state()[-1]
        f_u, f_v, g_u, g_v = self.reaction_jac(u, v, t) 
        return -f_u/eig_v
        
    def plot_pattern_space(self, sigma_min=0.0, sigma_max=100.0, num_plot=100, plot_equal_lines=False, t=0):
        'Return figure handle for the pattern space plot'
        u, v = self.steady_state()[0], self.steady_state()[-1]
        spectrum = self.spectrum
        fig = plt.figure() # initiate figure
        num_nodes = np.size(spectrum) # number of nodes on the graph
        # Vector with the asymptotical value for epsilon for each eigenvalue
        epsilon_max = 0.99*self.epsilon_maximum(spectrum)
        # Vector to store the intersection of the marginal stability curves
        epsilon_intersection = np.zeros(np.shape(spectrum))
        # No eps_max for the zero eigenvalue
        eps_max = None
        # Iterate over the spectrum
        for i, eig_v in enumerate(spectrum):
            # Skip any zero eigenvalues 
            if np.abs(eig_v) < 1e-13 or i == 0: 
                epsilon_max[i] = 0.0 
                continue 
            # Skip double eigenvalues 
            if i < num_nodes -1: 
                if np.abs(eig_v-spectrum[i+1]) < 1e-13: 
                    continue 
                # Run epsilon from previous intersection to new intersection 
            if eps_max == None:
                eps_max = epsilon_max[i]
            else:
                eps_max = eps_min
            if i == num_nodes-1: # if we are the latest eigenvalue we set the lower limit manually
                eps_min = 0.001
            else: # else the lower limit is the intersection point with the new curve
                epsilon_min = self.epsilon_curve(eig_v, sigma_max) # lower limit is the value of epsilon at sigma_max
                eps_min = self.intersection_bounding_curve(eig_v, spectrum[i+1], epsilon_min, epsilon_max[i+1])
            epsilon_intersection[i] = eps_min

            epsilon = np.linspace(eps_min,eps_max,num_plot)

            sigma_curve = self.bounding_curve(eig_v, epsilon)
            plt.plot(sigma_curve,epsilon,'k', label = 'l = %f' % eig_v)
            
            # If we want to fill the area
            y_fill = epsilon[sigma_curve <= 1.2*sigma_max]
            x_fill = sigma_curve[sigma_curve <= 1.2*sigma_max]
            if epsilon_max[i-1] == 0:
                x_fill = np.append(x_fill, x_fill[-1])
                y_fill = np.append(y_fill, y_fill[0])
            elif i == num_nodes-1:
                y_fill = epsilon[sigma_curve <= 1.5*sigma_max]
                x_fill = sigma_curve[sigma_curve <= 1.5*sigma_max]
                x_fill = np.append(x_fill, x_fill[0])
                y_fill = np.append(y_fill, y_fill[-1])
            else:
                x_fill = np.concatenate(([1.2*sigma_max],x_fill,[1.2*sigma_max]))
                y_fill = np.concatenate(([.8*y_fill[0]],y_fill,[1.2*y_fill[-1]]))
            plt.fill(x_fill,y_fill,'0.8',lw=1.0,edgecolor='0.8')
        # Stylise the plot
        plt.xlim([sigma_min, sigma_max])
        plt.ylim([0,max(epsilon_max)])
        plt.xlabel(r'$\sigma$')
        plt.ylabel(r'$\varepsilon$')
        # Plot lines of equal growth factor
        if plot_equal_lines == True:
            for i, epsilon in enumerate(epsilon_intersection[1:-1],1): # Loop over the intersections of marginal stability curves
                if epsilon == 0:
                    continue
                if i > 20: # Only do the first 20
                    continue
                sigma = np.linspace(self.bounding_curve(spectrum[i], epsilon), 100, num_plot)
                epsilon_crossing = epsilon_intersection[i]
                p = [self.equal_growth_factor(spectrum[i], spectrum[i+1], s, epsilon_crossing) 
                        for s in sigma]
                plt.plot(sigma,p,'0.3')
        ax = plt.gca()
#        ax.set_yscale('log')
        return fig

    def plot_sol_network(self, sol):
        'Return figure handle for plot of a solution vector on the network'
        fig = plt.figure()
        pos = nx.spring_layout(self.G)
        network_axes = fig.add_axes([0.0,0.0,0.9,1.0])
        colors = sol
        cmap = plt.get_cmap('bwr')
        vmin = -max(abs(colors))
        vmax = max(abs(colors))
        with warnings.catch_warnings(): # Ignore networkx draw warnings
            warnings.simplefilter("ignore", UserWarning)
            warnings.filterwarnings("ignore",category=matplotlib.cbook.mplDeprecation)
            nodes = nx.draw_networkx_nodes(self.G, pos, ax=network_axes, node_color=sol, cmap=cmap,
                    vmin=vmin, vmax=vmax)
            nodes.set_edgecolor('k')
            nx.draw_networkx_edges(self.G, pos, width=1.0)
            cbaxes = fig.add_axes([0.9, 0.1, 0.02, 0.8])
            sm = plt.cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(vmin=vmin, vmax=vmax))
            sm._A = []
            plt.colorbar(sm, cax=cbaxes)
        return fig

    def plot_ev_coef(self, sol, log_scale=False):
        'Return figure handle for plot of coefficients of eigenvector directions'
        G = self.G
        num_nodes = G.number_of_nodes()
        fig = plt.figure()
        L = nx.laplacian_matrix(G)
        V, W = np.linalg.eig(-L.todense())
        idx = V.argsort()[::-1]
        V = V[idx]
        W = W[:,idx]
        coef = W.transpose().dot(sol).transpose()
        ax = plt.gca()
        ax.stem(range(1,num_nodes+1),np.abs(coef))
        ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        if log_scale:
            ax.set_yscale('log')
        plt.xlim([0.5,num_nodes+0.5])
        plt.xlabel(r'Eigenvalue index')
        plt.ylabel(r'$|a_r|$')
        return fig

    def animate_solution(self, sol, T, sorted_index=None):
        'Plot the evolution of the concentration of one of the species'
        fig = plt.figure()
        num_nodes = self.G.number_of_nodes()
        time_steps = len(T)
        if sorted_index == None:
            sorted_index = range(num_nodes)
        ax = plt.axes(xlim=(-0.5,num_nodes-0.5), ylim=(0,np.ceil(np.max(sol))))
        line, = ax.plot([], [], marker='.', ls='', lw=2)
        time_text = ax.text(0.5,0.5,'', fontsize=15, transform=ax.transAxes)

        def init():
            line.set_data([],[])
            time_text.set_text(r'$T$ = 0')
            ax.set_xlabel(r'Node index')
            ax.set_ylabel(r'$u$')
            return line, time_text

        def animate(i):
            line.set_data(range(num_nodes),sol[i,:][sorted_index])
            time_text.set_text(r'$T$ = %f' % T[i])
            return line, time_text

        anim = animation.FuncAnimation(fig, animate, init_func=init, frames=time_steps, interval=150)
        return anim
