from mimura_murray import *
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import warnings
import random
from graph_definitions import triangle_lattice, ba_graph

## Look at the pattern space for various graphs

## Define the parameters for Mimura-Murray
a = 35.0; b = 16.0; c = 9.0; d = 2.0/5.0

## Network definition
np.random.seed(42) # set the random seed
num_nodes = 20 # Number of nodes
## Complete graph
G = nx.complete_graph(num_nodes)
## Rectangular lattice grid
G = nx.convert_node_labels_to_integers(nx.grid_2d_graph(4,5))
## Triangle lattice
G = triangle_lattice(4,5)
## Barabasi-Albert graph
deg_attach = 2
# initial complete graph
G = ba_graph(num_nodes, deg_attach)
# initial empty graph
G = nx.barabasi_albert_graph(num_nodes, deg_attach)

## Create instance of the model
problem = MimuraMurray(35.0, 16.0, 9.0, 2.0/5.0, G)

## Plot the pattern space
sigma_min = 0.0
sigma_max = 50.0
plot_equal_lines = True
f1 = problem.plot_pattern_space(sigma_min, sigma_max, plot_equal_lines=plot_equal_lines)
plt.ylabel(r'$\varepsilon$',fontsize=24)
plt.xlabel(r'$\sigma$',fontsize=24)
plt.tick_params(axis='both', which='major', labelsize=16)
#plt.savefig('TS_triangle.pdf')

## Plot the network
with warnings.catch_warnings(): # Ignore networkx draw warnings
    f2 = plt.figure()
    warnings.simplefilter("ignore", UserWarning)
    warnings.filterwarnings("ignore",category=matplotlib.cbook.mplDeprecation)
    nx.draw(G)
#    nx.draw_circular(G)
plt.show()
